require "arma_server_query/version"
require 'core_ext/stringio'
require 'errors/errors'
require 'packets/packets'
require 'steam_player'

module ArmaServerQuery

  class ServerInfo

    attr_reader :host, :port

    def initialize(host, port)
      @host = host
      @port = port
      @timeout = 1000
    end

    def query
      connect
      result = fetch_server_info
      disconnect
      result
    end

    private

    def connect
      puts "Connecting #{host}:#{port}"
      @socket = UDPSocket.new
      @socket.connect host, port
    end

    def fetch_server_info
      # Fetch server info
      info_sequence = [0xFF, 0xFF, 0xFF, 0xFF, 0x54, 0x53, 0x6F, 0x75, 0x72, 0x63, 0x65, 0x20, 0x45, 0x6E, 0x67, 0x69, 0x6E, 0x65, 0x20, 0x51, 0x75, 0x65, 0x72, 0x79, 0x00].pack('c*')
      @socket.send info_sequence, 0
      server_query_info_response = reply

      # Get player stats challenge number
      info_sequence = [0xFF, 0xFF, 0xFF, 0xFF, 0x55, 0xfe, 0xfd, 0x00, 0x43, 0x4f, 0x52, 0x59, 0x00, 0xff, 0xff].pack('c*')
      @socket.send info_sequence, 0
      challenge_response = reply

      # Get player informations
      request = A2S_PLAYER_Packet.new(challenge_response.challenge_number)
      @socket.send request.to_s, 0
      player_information_response = reply

      # Return data
      [server_query_info_response, player_information_response]
    end

    def receive_packet(buffer_length = 0)
      if select([@socket], nil, nil, @timeout / 1000.0).nil?
        raise ArmaServerQuery::TimeoutError
      end

      if buffer_length == 0
        @buffer.rewind
      else
        @buffer = StringIO.alloc buffer_length
      end

      begin
        data = @socket.recv @buffer.remaining
      rescue Errno::ECONNRESET
        @socket.close
        raise $!
      end
      bytes_read = @buffer.write data
      @buffer.truncate bytes_read
      @buffer.rewind

      bytes_read
    end

    def reply
      receive_packet 1400

      if @buffer.long == 0xFFFFFFFE
        split_packets = []
        begin
          request_id = @buffer.long
          packet_number_and_count = @buffer.getbyte
          packet_count = packet_number_and_count & 0xF
          packet_number = (packet_number_and_count >> 4) + 1

          split_packets[packet_number - 1] = @buffer.get

          puts "Received packet #{packet_number} of #{packet_count} for request ##{request_id}"

          if split_packets.size < packet_count
            begin
              bytes_read = receive_packet
            rescue ArmaServerQuery::TimeoutError
              bytes_read = 0
            end
          else
            bytes_read = 0
          end
        end while bytes_read > 0 && @buffer.long == 0xFFFFFFFE

        puts split_packets
        #packet = SteamCondenser::Servers::Packets::SteamPacketFactory.reassemble_packet(split_packets)
      else
        packet = packet_from_data(@buffer.get)
      end
      puts "Got reply of type \"#{packet.class.to_s}\"."
      packet
    end

    def packet_from_data(raw_data)
      header = raw_data[0].ord
      data = raw_data[1..-1]

      case header
      when BasePacket::S2A_INFO2_HEADER
        return S2A_INFO2_Packet.new(data)
      when BasePacket::S2A_PLAYER_HEADER
        return S2A_PLAYER_Packet.new(data)
      when BasePacket::S2C_CHALLENGE_HEADER
        return S2C_CHALLENGE_Packet.new(data)
      else
        raise UnknownPacketError.new("Unknown packet with header 0x#{header.to_s(16)} received.")
      end
    end

    def disconnect
      @socket.close
    end

  end


end
