require "arma_server_query/version"
require 'core_ext/stringio'
require 'errors/errors'

module ArmaServerQuery
  module BasePacket

    A2M_GET_SERVERS_BATCH2_HEADER = 0x31
    A2S_INFO_HEADER = 0x54
    A2S_PLAYER_HEADER = 0x55
    A2S_RULES_HEADER = 0x56
    A2S_SERVERQUERY_GETCHALLENGE_HEADER = 0x57
    C2M_CHECKMD5_HEADER = 0x4D
    M2A_SERVER_BATCH_HEADER = 0x66
    RCON_GOLDSRC_CHALLENGE_HEADER = 0x63
    RCON_GOLDSRC_NO_CHALLENGE_HEADER = 0x39
    RCON_GOLDSRC_RESPONSE_HEADER = 0x6c
    S2A_INFO_DETAILED_HEADER = 0x6D
    S2A_INFO2_HEADER = 0x49
    S2A_PLAYER_HEADER = 0x44
    S2A_RULES_HEADER = 0x45
    S2C_CONNREJECT_HEADER = 0x39
    S2C_CHALLENGE_HEADER = 0x41

    # Creates a new packet object based on the given data
    #
    # @param [Fixnum] header_data The packet header
    # @param [String] content_data The raw data of the packet
    def initialize(header_data, content_data = '')
      @content_data = StringIO.new content_data.to_s
      @header_data = header_data
    end

    # Returns the raw data representing this packet
    #
    # @return [String] A string containing the raw data of this request packet
    def to_s
      [0xFF, 0xFF, 0xFF, 0xFF, @header_data, @content_data.string].pack('c5a*')
    end

  end

  module S2A_INFO_BasePacket

    include BasePacket

    # Returns the information provided by the server
    #
    # @return [Hash<String, Object>] The information provided by the server
    def info
      @info ||= {}
    end

  end

  class S2A_INFO2_Packet

    include S2A_INFO_BasePacket

    EDF_GAME_ID = 0x01
    EDF_GAME_PORT = 0x80
    EDF_SERVER_ID = 0x10
    EDF_SERVER_TAGS = 0x20
    EDF_SOURCE_TV = 0x40

    # Creates a new S2A_INFO2 response object based on the given data
    #
    # @param [String] data The raw packet data replied from the server
    # @see S2A_INFO_BasePacket#generate_info_hash
    def initialize(data)
      super S2A_INFO2_HEADER, data

      info.merge!({
                      protocol_version: @content_data.getbyte,
                      server_name: @content_data.cstring,
                      map_name: @content_data.cstring,
                      game_directory: @content_data.cstring,
                      game_description: @content_data.cstring,
                      app_id: @content_data.short,
                      number_of_players: @content_data.getbyte,
                      max_players: @content_data.getbyte,
                      number_of_bots: @content_data.getbyte,
                      dedicated: @content_data.getc,
                      operating_system: @content_data.getc,
                      password_needed: @content_data.getbyte == 1,
                      secure: @content_data.getbyte == 1,
                      game_version: @content_data.cstring
                  })

      if @content_data.remaining > 0
        extra_data_flag = @content_data.getbyte

        if extra_data_flag & EDF_GAME_PORT != 0
          info[:server_port] = @content_data.short
        end

        if extra_data_flag & EDF_SERVER_ID != 0
          info[:server_id] = @content_data.long | (@content_data.long << 32)
        end

        if extra_data_flag & EDF_SOURCE_TV != 0
          info[:tv_port] = @content_data.short
          info[:tv_name] = @content_data.cstring
        end

        if extra_data_flag & EDF_SERVER_TAGS != 0
          info[:server_tags] = @content_data.cstring
        end

        if extra_data_flag & EDF_GAME_ID != 0
          info[:game_id] = @content_data.long | (@content_data.long << 32)
        end
      end
    end

  end

  module RequestWithChallenge

    # Returns the raw data representing this packet
    #
    # @return [String] A string containing the raw data of this request packet
    def to_s
      [0xFF, 0xFF, 0xFF, 0xFF, @header_data, @content_data.string.to_i].pack('c5l')
    end
  end

  class S2C_CHALLENGE_Packet

    include BasePacket

    # Creates a new S2C_CHALLENGE response object based on the given data
    #
    # @param [String] challenge_number The raw packet data replied from the
    #        server
    def initialize(challenge_number)
      super S2C_CHALLENGE_HEADER, challenge_number
    end

    # Returns the challenge number received from the game server
    #
    # @return [Fixnum] The challenge number provided by the game server
    def challenge_number
      @content_data.rewind
      @content_data.long
    end
  end

  class A2S_PLAYER_Packet

    include BasePacket
    include RequestWithChallenge

    # Creates a new A2S_PLAYER request object including the challenge number
    #
    # @param [Numeric] challenge_number The challenge number received from the
    #        server
    def initialize(challenge_number = -1)
      super A2S_PLAYER_HEADER, challenge_number
    end

  end

  # @see GameServer#update_player_info
  class S2A_PLAYER_Packet

    include BasePacket

    # Returns the list of active players provided by the server
    #
    # @return [Hash<String, SteamPlayer>] All active players on the server
    attr_reader :player_hash

    # Creates a new S2A_PLAYER response object based on the given data
    #
    # @param [String] content_data The raw packet data sent by the server
    # @raise [Error::PacketFormat] if the packet data is not well formatted
    def initialize(content_data)
      if content_data.nil?
        raise PacketFormatError.new('Wrong formatted S2A_PLAYER packet.')
      end

      super S2A_PLAYER_HEADER, content_data

      @content_data.getbyte
      @player_hash = {}

      while @content_data.remaining > 0
        player_data = @content_data.getbyte, @content_data.cstring, @content_data.signed_long, @content_data.float
        @player_hash[player_data[1]] = SteamPlayer.new(*player_data[0..3])
      end
    end

  end

end
