require "arma_server_query/version"
require 'core_ext/stringio'

module ArmaServerQuery

  class TimeoutError < StandardError
    def initialize
      super 'The operation timed out.'
    end
  end

  class UnknownPacketError < StandardError
    def initialize(msg)
      super msg
    end
  end

  class PacketFormatError < StandardError
    def initialize(msg)
      super msg
    end
  end

end
